package com.bosssoft.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LoginInterceptor implements HandlerInterceptor {

    public boolean preLogin(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        System.out.println(username+" "+password);

        if(username.equals("venb") && password.equals("123456")) {
            return true;
        } else {
            req.setAttribute("msg", "登录用户名或密码错误");
            req.getRequestDispatcher("index.jsp").forward(req, res);
            return false;
        }
    }
}
