package com.bosssoft.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class HelloController {

    //测试@RequestMapping
    @RequestMapping("/test1")
    public String test1(){
        System.out.println("hello");
        return "success";
    }
    //测试RequestBody
    @RequestMapping("/test2")
    public String test2(@RequestBody String body){
        System.out.println(body);
        return "success";
    }
    //测试PathVariable
    @RequestMapping("/test3/{id}")
    public String test3(@PathVariable("id")int id){
        System.out.println(id);
        return "success";
    }
    //测试RequestHeader
    @RequestMapping("/test4")
    public String test4(@RequestHeader(value="Accept") String header){
        System.out.println(header);
        return "success";
    }
    //测试CookieValue
    @RequestMapping("/test5")
    public String test5(@CookieValue(value="JSESSIONID") String cookieValue){
        System.out.println(cookieValue);
        return "success";
    }
    //测试RequestParam
    @RequestMapping("/test6")
    public String test6(@RequestParam(value="username",required=false)String name){
        System.out.println(name);
        return "success";
    }

}
