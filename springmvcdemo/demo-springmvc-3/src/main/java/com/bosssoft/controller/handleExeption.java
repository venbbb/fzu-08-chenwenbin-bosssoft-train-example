package com.bosssoft.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class handleExeption {

    @RequestMapping("/test")
    public int testExecption(){
        int i = 1/0;
        return i;
    }

    @ExceptionHandler()
    public String handleException(Exception ex) {
        System.out.println("抛异常了:" + ex);
        ex.printStackTrace();
        String resultStr = "异常：默认";
        return resultStr;
    }
}
