package com.bosssoft.aop;


import com.bosssoft.anno.IdCheck;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Aspect
@Component
public class IdCheckAop {

    @Pointcut("execution(* com.bosssoft.controller.*.*(..))")
    public void pointcut(){}


    @Around("pointcut()")
    public Object idCheck(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("进行id检查");
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();

        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        if (parameterAnnotations==null||parameterAnnotations.length==0){
            return pjp.proceed();
        }
        Object[] parameterValues = pjp.getArgs();
        for(int i=0;i<parameterAnnotations.length;i++){
            for (int j=0;j<parameterAnnotations[i].length;j++){
                if (parameterAnnotations[i][j]!=null && parameterAnnotations[i][j]instanceof IdCheck){
                    if(parameterValues[i]==null||"".equals(parameterValues[i].toString())){
                        writeContent("id为空或不存在");
                    }
                    break;
                }
            }
        }
        return pjp.proceed();
    }

    //将异常输出到页面
    private void writeContent(String content) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        response.reset();
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Type", "text/plain;charset=UTF-8");
        response.setHeader("icop-content-type", "exception");
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.print(content);
        writer.flush();
        writer.close();
    }
}
