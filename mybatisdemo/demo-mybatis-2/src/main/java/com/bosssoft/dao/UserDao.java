package com.bosssoft.dao;

import com.bosssoft.domain.User;

public interface UserDao {

    User getUserByIdWithCompany(Integer id);
    User getUserByIdWithRole(Integer id);
}
