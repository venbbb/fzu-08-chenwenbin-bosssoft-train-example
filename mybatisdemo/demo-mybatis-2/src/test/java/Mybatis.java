import com.bosssoft.dao.UserDao;
import com.bosssoft.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Mybatis {
    public static void main(String[] args) throws IOException {
        InputStream in = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(in);
        SqlSession session = factory.openSession();

        UserDao userDao = session.getMapper(UserDao.class);

        User user = userDao.getUserByIdWithCompany(1);
        System.out.println(user);

        User user1 = userDao.getUserByIdWithRole(1);
        System.out.println(user1);

        session.close();
    }
}
