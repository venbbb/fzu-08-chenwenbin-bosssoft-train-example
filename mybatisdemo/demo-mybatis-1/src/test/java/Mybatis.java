import com.bosssoft.dao.IUserDao;
import com.bosssoft.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Mybatis {
    public static void main(String[] args) throws IOException {
        InputStream in = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(in);
        SqlSession session = factory.openSession();
        IUserDao userDao = session.getMapper(IUserDao.class);

        System.out.println("第一次查询");
        List<User> users = userDao.findAll();
        for (User user:users){
            System.out.println(user);
        }

        System.out.println("第二次查询");
        List<User> users2 = userDao.findAll();
        for (User user:users2){
            System.out.println(user);
        }

    }
}
