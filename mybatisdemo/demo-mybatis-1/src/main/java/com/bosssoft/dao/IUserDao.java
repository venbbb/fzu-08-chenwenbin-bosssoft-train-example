package com.bosssoft.dao;

import com.bosssoft.domain.User;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@CacheNamespace(blocking = true)
public interface IUserDao {

    @Select("select * from t_user")
    List<User> findAll();

    @Select("select * from t_user where id=#{id}")
    List<User> findById(int id);
}
