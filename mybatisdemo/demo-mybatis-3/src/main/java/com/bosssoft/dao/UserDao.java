package com.bosssoft.dao;

import com.bosssoft.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    void insertUsers(@Param("users") List<User> users);
    List<User> selectUsersByCondition(User user);
    void updateUser(User user);
}
