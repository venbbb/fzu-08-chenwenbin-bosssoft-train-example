import com.bosssoft.dao.UserDao;
import com.bosssoft.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Mybatis {
    public static void main(String[] args) throws IOException {
        InputStream in = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(in);
        SqlSession session = factory.openSession();

        UserDao userDao = session.getMapper(UserDao.class);

//        List<User> users = new ArrayList<User>();
//        User user;
//        for (int i = 0; i < 5; i++) {
//            user = new User(i,"venbbb");
//            users.add(user);
//        }
//        userDao.insertUsers(users);
//
//        User user = new User(0,"venb");
//        userDao.updateUser(user);

        User user = new User(0,"venbbb");
        List<User> users = userDao.selectUsersByCondition(user);
        System.out.println(users);
        session.commit();
        session.close();
    }
}
