import com.bosssoft.dao.UserMapper;
import com.bosssoft.domain.UserExample;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import com.bosssoft.domain.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Mybatis {
    public static void main(String[] args) throws IOException {
        InputStream in = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(in);
        SqlSession session = factory.openSession();

        UserMapper userMapper =session.getMapper(UserMapper.class);
//        User user;
//        for (int i = 0; i < 5; i++) {
//            user = new User(i,"venbb");
//            userMapper.insert(user);
//        }
//        UserExample example = new UserExample();
//        UserExample.Criteria criteria = example.createCriteria();
//
//        criteria.andIdEqualTo(0).andNameEqualTo("venbb");
//        List<User> users = userMapper.selectByExample(example);
//        System.out.println(users);

        User user = new User(0,"venb");
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andIdEqualTo(0);
        userMapper.updateByExample(user,userExample);
        session.commit();
        session.close();

    }
}
