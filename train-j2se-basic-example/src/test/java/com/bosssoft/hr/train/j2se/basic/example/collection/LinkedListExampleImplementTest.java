package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListExampleImplementTest {

    @Test
    public void addFirst() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.addFirst(new User(1,"A"));
        linkedListExampleImplement.addFirst(new User(2,"B"));
        linkedListExampleImplement.addFirst(new User(3,"C"));
        assertEquals(3,linkedListExampleImplement.size());
    }

    @Test
    public void offer() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.addFirst(new User(1,"A"));
        linkedListExampleImplement.addFirst(new User(2,"B"));
        assertTrue(linkedListExampleImplement.offer(new User(3,"C")));
    }

    @Test
    public void sychronizedVisit() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.sychronizedVisit(new User(1,"A"));
        linkedListExampleImplement.sychronizedVisit(new User(2,"B"));
        assertEquals(linkedListExampleImplement.size(),2);
    }

    @Test
    public void push() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.push(new User(1,"A"));
        linkedListExampleImplement.push(new User(2,"B"));
        assertEquals(linkedListExampleImplement.size(),2);
    }

    @Test
    public void pop() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.push(new User(1,"A"));
        linkedListExampleImplement.push(new User(2,"B"));
        assertEquals(linkedListExampleImplement.size(),2);
        assertEquals(linkedListExampleImplement.pop(),new User(2,"B"));
        assertEquals(linkedListExampleImplement.pop(),new User(1,"A"));
    }

    @Test
    public void append() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.append(new User(1,"A"));
        linkedListExampleImplement.append(new User(2,"B"));
        assertEquals(linkedListExampleImplement.size(),2);
    }

    @Test
    public void get() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.append(new User(1,"A"));
        linkedListExampleImplement.append(new User(2,"B"));
        assertEquals(linkedListExampleImplement.get(1),new User(2,"B"));
    }

    @Test
    public void remove() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.append(new User(1,"A"));
        linkedListExampleImplement.append(new User(2,"B"));
        linkedListExampleImplement.remove(1);
        assertEquals(linkedListExampleImplement.size(),1);
    }

    @Test
    public void listByIndex() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.append(new User(1,"A"));
        linkedListExampleImplement.append(new User(2,"B"));
        linkedListExampleImplement.listByIndex();
    }

    @Test
    public void listByIterator() {
        LinkedListExampleImplement linkedListExampleImplement = new LinkedListExampleImplement();
        linkedListExampleImplement.append(new User(1,"A"));
        linkedListExampleImplement.append(new User(2,"B"));
        linkedListExampleImplement.listByIterator();
    }

    @Test
    public void toArray() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(1,"A"));
        arrayListExampleImplement.append(new User(2,"B"));
        arrayListExampleImplement.append(new User(3,"C"));
        User[] users = new User[3];
        users[0] = new User(1,"A");
        users[1] = new User(2,"B");
        users[2] = new User(3,"C");
        assertEquals(arrayListExampleImplement.toArray(), users);
    }

    @Test
    public void sort() {
        ArrayListExampleImplement arrayListExampleImplement = new ArrayListExampleImplement();
        arrayListExampleImplement.append(new User(3,"A"));
        arrayListExampleImplement.append(new User(1,"B"));
        arrayListExampleImplement.append(new User(2,"C"));
        arrayListExampleImplement.sort();
    }
}