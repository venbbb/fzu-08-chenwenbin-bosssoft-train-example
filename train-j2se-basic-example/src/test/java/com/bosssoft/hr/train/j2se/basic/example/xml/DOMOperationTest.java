package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static org.junit.Assert.*;

public class DOMOperationTest {

    @Test
    public void testCreate() {
        DOMOperation domOperation = new DOMOperation();
        Student student = new Student(4,"Ann",20);
        domOperation.create(student);
    }

    @Test
    public void testRemove() {
        DOMOperation domOperation = new DOMOperation();
        Student student = new Student(4,"Ann",20);
        domOperation.remove(student);
    }

    @Test
    public void testUpdate() {
        DOMOperation domOperation = new DOMOperation();
        Student student = new Student(3,"Lee",30);
        domOperation.update(student);
    }

    @Test
    public void testQuery() {
        DOMOperation domOperation = new DOMOperation();
        Student student = new Student(1,null,0);
        Student queryRes = domOperation.query(student);
        System.out.println(queryRes);
    }
}