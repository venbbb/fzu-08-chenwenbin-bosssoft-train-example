package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class StackExampleImplementTest {

    @Test
    public void push(){
        StackExampleImplement stackExampleImplement = new StackExampleImplement();
        stackExampleImplement.push(new User(1,"a"));
        stackExampleImplement.push(new User(2,"b"));
        stackExampleImplement.push(new User(3,"c"));
        assertEquals(3,stackExampleImplement.size());
    }

    @Test
    public void pop(){
        StackExampleImplement stackExampleImplement = new StackExampleImplement();
        stackExampleImplement.push(new User(1,"a"));
        stackExampleImplement.push(new User(2,"b"));
        stackExampleImplement.pop();
        assertEquals(1,stackExampleImplement.size());
    }
}
