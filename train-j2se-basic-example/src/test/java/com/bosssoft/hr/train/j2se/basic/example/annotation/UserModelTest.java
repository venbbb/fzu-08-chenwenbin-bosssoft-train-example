package com.bosssoft.hr.train.j2se.basic.example.annotation;

import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class UserModelTest {

    @Test
    public void testSave() {
        try{
            UserModel userModel = new UserModel(4,"Charles",22);
            userModel.save();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdate() {
        try{
            UserModel userModel = new UserModel(4,"Charles",22);
            userModel.update();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testRemove() {
        try{
            UserModel userModel = new UserModel(4,"Charles",22);
            userModel.remove();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testQueryForList() {
        try{
            UserModel userModel = new UserModel(4,"Charles",22);
            userModel.queryForList();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}