package com.bosssoft.hr.train.j2se.basic.example.thread;

/**
   线程池的接口
 */
public interface Executor {

    void execute(Runnable command);
}
