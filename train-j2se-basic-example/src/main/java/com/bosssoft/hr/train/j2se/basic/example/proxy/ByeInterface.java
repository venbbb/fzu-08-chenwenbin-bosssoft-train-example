package com.bosssoft.hr.train.j2se.basic.example.proxy;

public interface ByeInterface {
    void sayByeBye();
}
