package com.bosssoft.hr.train.j2se.basic.example.proxy;

public interface HelloWorld {
    void sayHello();
}
