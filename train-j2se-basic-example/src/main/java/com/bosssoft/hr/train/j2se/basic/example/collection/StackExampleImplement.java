package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import lombok.extern.slf4j.Slf4j;

import java.util.Stack;

@Slf4j
public class StackExampleImplement implements StackExmaple<User> {

    private Stack<User> stack = new Stack<>();

    @Override
    public void push(User e) {
        stack.push(e);
    }

    @Override
    public User pop() {
        return stack.pop();
    }

    @Override
    public boolean empty() {
        return stack.isEmpty();
    }

    @Override
    public int search(User e) {
        return stack.search(e);
    }

    @Override
    public User peek() {
        return stack.peek();
    }

    public int size(){
        return stack.size();
    }
}
