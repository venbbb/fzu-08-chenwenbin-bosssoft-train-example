package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:13
 * @since
 **/
@Slf4j
public class DOMOperation implements XMLOperation<Student> {

    private String xmlPath = "src/main/java/com/bosssoft/hr/train/j2se/basic/example/xml/student.xml";

    @Override
    public boolean create(Student object) {
        // 创建文件工厂实例
        String idValue = object.getId()+"";
        String nameValue = object.getName();
        String ageValue = object.getAge()+"";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringElementContentWhitespace(false);
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            // 创建Document对象
            Document xmldoc = db.parse(xmlPath);
            // 获取根节点
            Element root = xmldoc.getDocumentElement();
            // 创建节点child
            Element child = xmldoc.createElement("student");
            child.setAttribute("id", idValue+"");
            // 创建节点name
            Element name = xmldoc.createElement("name");
            name.setTextContent(nameValue);
            child.appendChild(name);
            // 创建节点name
            Element age = xmldoc.createElement("age");
            age.setTextContent(ageValue);
            child.appendChild(age);
            // 把child添加到根节点中
            root.appendChild(child);
            // 保存
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer former = factory.newTransformer();
            former.transform(new DOMSource(xmldoc), new StreamResult(new File(
                    xmlPath)));
        } catch (Exception e) {
            log.info(e.toString());
            return false;
        }
        return true;
    }

    @Override
    public boolean remove(Student object){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringElementContentWhitespace(true);
        String idValue = object.getId()+"";
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xmldoc = db.parse(xmlPath);
            // 获取根节点
            Element root = xmldoc.getDocumentElement();
            // 定位根节点中的子节点
            Element son = (Element) selectSingleNode("/students/student[@id="+idValue+"]",
                    root);
            // 删除该节点
            root.removeChild(son);
            // 保存
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer former = factory.newTransformer();
            former.transform(new DOMSource(xmldoc), new StreamResult(new File(
                    xmlPath)));

        } catch (Exception e) {
            log.info(e.toString());
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Student object) {
        this.remove(object);
        return this.create(object);
    }

    @Override
    public Student query(Student object) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringElementContentWhitespace(true);
        String idValue = object.getId()+"";
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xmldoc = db.parse(xmlPath);
            // 获取根节点
            Element root = xmldoc.getDocumentElement();
            int id = object.getId();
            String name = object.getName();
            int age = object.getAge();
            if(id != 0){
                Element per = (Element) selectSingleNode("/students/student[@id="+id+"]",
                        root);
                if (per == null){
                    return null;
                }
                // 修改值
                String newAge = per.getElementsByTagName("age").item(0).getNodeValue();
                // 修改值
                String newName = per.getElementsByTagName("name").item(0).getNodeValue();
                return new Student(id,newName,Integer.parseInt(newAge));
            } else if(name != null){
                Element per = (Element) selectSingleNode("/students/student[@name="+name+"]",
                        root);
                if (per == null){
                    return null;
                }
                // 修改值
                String newId = per.getElementsByTagName("id").item(0).getNodeValue();
                // 修改值
                String newAge = per.getElementsByTagName("age").item(0).getNodeValue();
                return new Student(Integer.parseInt(newId),name,Integer.parseInt(newAge));
            } else if(age != 0){
                Element per = (Element) selectSingleNode("/students/student[@age="+age+"]",
                        root);
                if (per == null){
                    return null;
                }
                // 修改值
                String newId = per.getElementsByTagName("id").item(0).getNodeValue();
                // 修改值
                String newName = per.getElementsByTagName("name").item(0).getNodeValue();
                return new Student(Integer.parseInt(newId),newName,age);
            }
        } catch (Exception e) {
            log.info(e.toString());
        }
        return null;
    }

    private static Node selectSingleNode(String express, Element source) {
        Node result = null;
        //创建XPath工厂
        XPathFactory xpathFactory = XPathFactory.newInstance();
        //创建XPath对象
        XPath xpath = xpathFactory.newXPath();
        try {
            result = (Node) xpath.evaluate(express, source, XPathConstants.NODE);
            log.info(result.toString());
        } catch (XPathExpressionException e) {
            log.info(e.toString());
        }

        return result;
    }
}
