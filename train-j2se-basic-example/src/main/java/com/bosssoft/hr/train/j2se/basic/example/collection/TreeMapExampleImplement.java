package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class TreeMapExampleImplement implements TreeMapExample<Role, Resource>{

    Map<Role,Resource> map = new TreeMap<>(new Comparator<Role>() {
        @Override
        public int compare(Role o1, Role o2) {
            return o1.getId() - o2.getId();
        }
    });

    /**
     * @param key   map的键
     * @param value 值
     * @return 返回值
     */
    @Override
    public Resource put(Role key, Resource value) {
        return map.put(key,value);
    }

    /**
     * @param key map的键
     * @return 返回值
     */
    @Override
    public Resource remove(Role key) {
        return map.remove(key);
    }

    public int size(){
        return map.size();
    }

    /**
     * @param key map的键
     * @return 返回值 是否存在的判断
     */
    @Override
    public boolean containsKey(Role key) {
        return map.containsKey(key);
    }

    /**
     * 迭代方式1
     */
    @Override
    public void visitByEntryset() {
        Set<Map.Entry<Role,Resource>> set = map.entrySet();
        for(Map.Entry<Role,Resource> entry:set){
            log.info(Constraint.LOG_TAG + "   " + entry.getKey() + " " + entry.getValue());
        }
    }

    /**
     * 迭代方式2
     */
    @Override
    public void visitByKeyset() {
        if (null != map){
            Set<Role> set = map.keySet();
            Iterator<Role> keyIter = set.iterator();
            Role role = keyIter.next();
            for(;keyIter.hasNext();role = keyIter.next()){
                log.info(Constraint.LOG_TAG + "   " + role + " " + map.get(role));
            }
        }
    }

    /**
     * 迭代方式3
     */
    @Override
    public void visitByValues() {
        if (null != map){
            Collection<Resource> collection = map.values();
            Iterator<Resource> valueIter = collection.iterator();
            Resource resource = valueIter.next();
            for(;valueIter.hasNext();resource = valueIter.next()){
                log.info(Constraint.LOG_TAG + "   "+ resource);
            }
        }
    }
}