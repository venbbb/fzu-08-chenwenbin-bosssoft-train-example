package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

@Slf4j
public class TreeSetExampleImplement implements TreeSetExmaple<User> {

    private Set<User> set = new TreeSet<>(
            new Comparator<User>(){
                @Override
            public int compare(User o1, User o2) { return o1.getId()-o2.getId(); }
            });
    /**
     * 测试实现对数据排序
     *
     * @param array
     * @return 已经排序的数组
     */
    @Override
    public User[] sort(User[] array) {
        if (null != array && array.length > 0){
            for(User user:array){
                set.add(user);
            }
            Object[] objects = set.toArray();
            User[] users = new User[objects.length];
            for(int i=0;i<objects.length;i++){
                users[i] = (User)objects[i];
            }
        }
        return null;
    }
}
