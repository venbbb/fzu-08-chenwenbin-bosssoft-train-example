package com.bosssoft.hr.train.j2se.basic.example.thread;

import java.util.logging.Logger;

/**
 * 丢弃当前线程
 */
public class DiscardRejectPolicy implements RejectPolicy {

    private static final Logger logger = Logger.getLogger(DiscardRejectPolicy.class.getName());
    @Override
    public void reject(Runnable task, ThreadPool threadPool) {
        logger.info("丢弃当前线程");
    }
}
