package com.bosssoft.hr.train.j2se.basic.example.proxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Hello implements HelloWorld {
    @Override
    public void sayHello() {
        log.info("Hello");
    }
}
