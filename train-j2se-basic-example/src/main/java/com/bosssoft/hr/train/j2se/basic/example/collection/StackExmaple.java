package com.bosssoft.hr.train.j2se.basic.example.collection;

public interface StackExmaple<T> {
    void push(T e);
    T pop();
    boolean empty();
    int search(T e);
    T peek();
}
