package com.bosssoft.hr.train.j2se.basic.example.proxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Bye implements ByeInterface{

    @Override
    public void sayByeBye() {
        log.info("ByeBye");
    }
}
