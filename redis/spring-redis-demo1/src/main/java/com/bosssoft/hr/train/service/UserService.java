package com.bosssoft.hr.train.service;

import com.bosssoft.hr.train.entity.User;

import java.util.List;

public interface UserService {

    Object findAll();
    User getUserById(Integer id);
}
