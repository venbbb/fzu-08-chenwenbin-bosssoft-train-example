package com.bosssoft.hr.train.mapper;

import com.bosssoft.hr.train.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    @Select("SELECT * FROM t_user WHERE id=#{id}")
    User getUserById(Integer id);
}
