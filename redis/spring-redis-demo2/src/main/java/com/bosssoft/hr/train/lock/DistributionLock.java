package com.bosssoft.hr.train.lock;

public interface DistributionLock {

    boolean lock(String key, String requestId, int expire);

    boolean release(String key, String requestId);
}
