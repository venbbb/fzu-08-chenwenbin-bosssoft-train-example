package com.bosssoft.hr.train.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class User implements Serializable {
    private static final long serialVersionUID = 1692718854911078569L;
    private Integer id;
    private String name;
    private Integer age;
}
