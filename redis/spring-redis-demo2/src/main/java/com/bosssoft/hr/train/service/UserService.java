package com.bosssoft.hr.train.service;

import com.bosssoft.hr.train.entity.User;

public interface UserService {
    User getUserById(Integer id);
}
