package com.bosssoft.hr.train.controller;

import com.bosssoft.hr.train.entity.User;
import com.bosssoft.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/getUser")
    public User getUserById(Integer id) {
        return userService.getUserById(id);
    }
}
