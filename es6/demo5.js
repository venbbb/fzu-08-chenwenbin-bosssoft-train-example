class Person{
    constructor(name){
        this.name = name
    }
}

class Student extends Person{
    constructor(name,id){
        super(name)
        this.id = id
    }
    getName(){
        return this.name
    }
    getId(){
        return this.id
    }
    setName(name){
        this.name = name
    }
    setId(id){
        this.id = id
    }
}

let student = new Student(1,"venb")
console.log(student.getName());
console.log(student.getId());
student.setName("venbb")
student.setId("2")
console.log(student.getName());
console.log(student.getId());

