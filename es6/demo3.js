class Student{
    constructor(id,name){
        this.id = id
        this.name = name
    }
}
class Grade{
    constructor(id,chinese,math,english){
        this.id = id
        this.chinese = chinese
        this.math = math
        this.english = english
    }
}
class StudentInfo{
    constructor(id,name,chinese,math,english){
        this.id = id
        this.name = name
        this.chinese = chinese
        this.math = math
        this.english = english
    }
}
let stu1 = new Student(1,"venb")
let stu2 = new Student(2,"venbb")
let stu3 = new Student(3,"venbbb")
let grade1 = new Grade(1,100,100,100)
let grade2 = new Grade(2,90,90,90)
let grade3 = new Grade(3,80,80,80)

let studentArr = [stu1,stu2,stu3]
let gradeArr = [grade1,grade2,grade3]

let result = []

for(let i of studentArr){
    for(let j of gradeArr){
        if(i.id === j.id){
            let id = i.id
            let name = i.name
            let chinese = j.chinese
            let math = j.math
            let english = j.english
            let studentInfo = new StudentInfo(id,name,chinese,math,english)
            result.push(studentInfo)
        }
    }
}

console.log(result);
