class Student{
    constructor(name){
        this.name = name
    }
    total(...grade){
        let sum = 0
        for(let i of grade){
            sum += i
        }
        return sum
    }
}

let stu1 = new Student("venb")
console.log(stu1.total(90,80));
console.log(stu1.total(90,80,60));
