<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="r" uri="/WEB-INF/tag.tld" %>
<!DOCTYPE html>
<html xml:lang="aa">
<head>
    <meta charset="UTF-8">
    <title>查询结果</title>

</head>
<body>
<table>
    <caption>查询结果</caption>
    <tr>
        <th scope="col">
            编号
        </th>
        <th scope="col">
            姓名
        </th>
        <th scope="col">
            年龄
        </th>
    </tr>
    </thead>
    <tbody>

    <%
        List<User> users = (List) session.getAttribute("users");
        for (User user : users) {
    %>
    <tr>
        <td><%=user.getId()%>
        </td>
        <td><%=user.getName()%>
        </td>
        <td><%=user.getCode()%>
        </td>
        <td><%=user.getCode()%>
        </td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>
<a href="UserListServlet">返回</a>
</body>
</html>
