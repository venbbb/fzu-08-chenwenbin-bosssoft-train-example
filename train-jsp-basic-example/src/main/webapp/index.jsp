<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html xml:lang="aa">
<head>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>
        $(function () {
            $("#login").click(function () {
                var username = $("#username").val();
                var password = $("#password").val();
                if (!username) {
                    alert("用户名必填!");
                    $("#username").focus();
                    return;
                }
                if (!password) {
                    alert("密码必填!");
                    $("#password").focus();
                    return;
                }
                $.ajax({
                    url: "login",
                    type: "post",
                    data: {
                        username: username,
                        password: password
                    },
                    dataType: "text",
                    success: function (data) {
                        if (data === '1') {
                            window.location.href = "userlist?name="+username+"&pwd="+password;
                        } else {
                            alert("用户名或密码错误!请重新输入!");
                        }
                    },
                    error: function () {
                        alert("错误");
                    }
                });
            });
        });
    </script>
    <title>登录</title>
</head>
<body>
    <h1>登录</h1>

    <table>
        <Caption>信息输入窗口</Caption>
        <tr>
            <th scope = "row">账号:</th>
            <th scope = "row">密码:</th>
        </tr>
        <tr>
            <td><input type="text" class="loginform_input" id="username"></td>
        </tr>
        <tr>
            <td><input type="password" class="loginform_input" id="password">
            </td>
        </tr>
    </table>
    <br>
    <input id="login" type="submit" class="loginform_submit" value="登录">
</body>
</html>