<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>

<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html xml:lang="aa">
<head>
    <meta charset="UTF-8">
    <title>查询用户</title>

</head>
<style type="text/css">
    .input1 {
        border: 1px solid #CDC28D;
        background-color: #F2F9FD;
        height: 40px;
        width: 200px;
        padding-top: 4px;
        font-size: 12px;
        padding-left: 10px;
    }

    .input2 {
        border: 1px solid #1E1E1E;
        background-color: #F2F9FD;
        height: 30px;
        width: 80px;
    }
</style>

<body>
<form action="QueryUserController" method="post">
    <table>
        <CAPTION>搜索信息</CAPTION>
        <tr>
            <th scope="row">ID:</th>
            <th scope="row">姓名:</th>
            <th scope="row">代码:</th>
            <th scope="row">密码:</th>
        </tr>
        <tr>
            <td colspan="2"><h1>查询用户</h1></td>
        </tr>
        <tr>
            <td><input class="input1" type="text" name="id"/></td>
        </tr>
        <tr>
            <td><input class="input1" type="text" name="name"/></td>
        </tr>
        <tr>
            <td><input class="input1" type="text" name="code"/></td>
        </tr>
        <tr>
            <td><input class="input1" type="text" name="password"/></td>
        </tr>
        <tr>
            <td colspan="2"><input class="input2" type="submit" value="查询"/></td>
        </tr>
    </table>
</form>
</body>
</html>
