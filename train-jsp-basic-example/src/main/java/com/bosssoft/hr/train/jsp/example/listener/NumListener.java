package com.bosssoft.hr.train.jsp.example.listener;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class NumListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent sessionEvent) {
        ServletContext servletContext = sessionEvent.getSession().getServletContext();
        Integer onlineCount = (Integer)servletContext.getAttribute("OnlineCount");
        onlineCount = onlineCount == null ? 1 : onlineCount + 1;
        servletContext.setAttribute("OnlineCount", onlineCount);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent sessionEvent) {
        ServletContext servletContext = sessionEvent.getSession().getServletContext();
        Integer onlineCount = (Integer)servletContext.getAttribute("OnlineCount");
        servletContext.setAttribute("OnlineCount", --onlineCount);
    }
}
