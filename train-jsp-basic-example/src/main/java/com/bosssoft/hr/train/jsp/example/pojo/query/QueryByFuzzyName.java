package com.bosssoft.hr.train.jsp.example.pojo.query;


import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryByFuzzyName extends Query{

    private static String sql = "select * from t_user where name like ";

    public QueryByFuzzyName(String fuzzyName) {
        sql = sql + "'" + fuzzyName + "'";
    }

    @Override
    public ResultSet query() {
        return DBUtil.query(sql);
    }
}