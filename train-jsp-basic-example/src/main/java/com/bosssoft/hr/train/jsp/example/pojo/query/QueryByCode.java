package com.bosssoft.hr.train.jsp.example.pojo.query;

import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryByCode extends Query{

    private static String sql = "select * from t_user where code=";
    public QueryByCode(String code) {
        sql += code;
    }

    @Override
    public ResultSet query() {
        return DBUtil.query(sql);
    }
}
