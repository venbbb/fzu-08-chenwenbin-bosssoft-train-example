package com.bosssoft.hr.train.jsp.example.pojo.query;

import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;

public class QueryById extends Query {

    private static String sql = "select * from t_user where id=";
    public QueryById(int id) {
        sql += id;
    }

    @Override
    public ResultSet query() {
        return DBUtil.query(sql);
    }
}
