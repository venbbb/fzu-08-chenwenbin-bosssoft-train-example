package com.bosssoft.hr.train.jsp.example.pojo.query;

import java.sql.ResultSet;

public abstract class Query{
    public abstract ResultSet query();
}
