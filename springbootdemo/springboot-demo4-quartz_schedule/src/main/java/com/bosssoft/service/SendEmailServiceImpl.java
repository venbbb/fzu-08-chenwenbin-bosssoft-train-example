package com.bosssoft.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
public class SendEmailServiceImpl implements SendEmailService{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${spring.mail.username}")
    private String fromEmail;

    //注入Spring发邮件的对象
    @Autowired
    private JavaMailSender javaMailSender;


    @Override
    @Scheduled(cron = "0/30 * * * * ?")
    public void sendEmail() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        //目标邮箱
        simpleMailMessage.setTo("1136500285@qq.com");
        simpleMailMessage.setFrom(fromEmail);

        //主题
        simpleMailMessage.setSubject("发送邮件测试");
        simpleMailMessage.setText("通过springboot定时发送邮件");
        try {
            javaMailSender.send(simpleMailMessage);
            logger.info("发送邮件成功");
        } catch (Exception e) {
            logger.info("发送邮件失败");
        }
    }
}
