package com.bosssoft.controller;

import com.bosssoft.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebServicesController {

    @Autowired
    private WebService webService;

    @GetMapping("/webservice")
    public Object get(@RequestParam("word") String word) {
        String webUrl = "http://www.webxml.com.cn/WebServices/TranslatorWebService.asmx?wsdl";
        String methodName = "getEnCnTwoWayTranslator";

        return webService.callWebService(webUrl, methodName, word);
    }
}
