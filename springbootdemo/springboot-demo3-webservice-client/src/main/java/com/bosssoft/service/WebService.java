package com.bosssoft.service;


public interface WebService {

    Object callWebService(String wsdUrl, String operationName, String... params);
}
