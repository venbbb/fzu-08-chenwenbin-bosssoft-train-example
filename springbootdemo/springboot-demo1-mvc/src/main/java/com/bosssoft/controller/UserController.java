package com.bosssoft.controller;

import com.bosssoft.domain.User;
import com.bosssoft.domain.UserExample;
import com.bosssoft.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserMapper userMapper;


    @GetMapping("/addUser")
    public String addUser(){
        return "add";
    }
    @PostMapping("/success")
    public String submit(@RequestParam(name = "id")Integer id,
                               @RequestParam(name = "name")String name,
                               @RequestParam(name = "age")Integer age){
        User user = new User(id,name,age);
        userMapper.insert(user);
        return "redirect:list";
    }

    @RequestMapping("/delete")
    public String deleteUser(@RequestParam("id") Integer id){
        userMapper.deleteByPrimaryKey(id);
        return "redirect:list";
    }

    @PostMapping("/update")
    public String update(@RequestParam(name = "id")Integer id,
                         @RequestParam(name = "name")String name,
                         @RequestParam(name = "age")Integer age){
        User user = new User(id,name,age);
        userMapper.updateByPrimaryKey(user);
        return  "redirect:list";
    }

    @GetMapping("update")
    public String updateUser(@RequestParam("id") Integer id,
                             @RequestParam("name")String name,
                             @RequestParam("age")Integer age,Model model){
        model.addAttribute("id",id);
        model.addAttribute("name",name);
        model.addAttribute("age",age);
        return "update";
    }


    @RequestMapping("/list")
    public ModelAndView showUserList(){
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andIdIsNotNull();
        List<User> users = userMapper.selectByExample(userExample);

        return new ModelAndView("userList","userList",users);
    }


}
