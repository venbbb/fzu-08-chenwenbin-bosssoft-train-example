package com.bosssoft.hr.train.dao;

import com.bosssoft.hr.train.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {

    @Autowired
    private  JdbcTemplate jdbcTemplate;

    public void addUser(User user){
        String sql = "INSERT tb_user (name) values (?)";
        jdbcTemplate.update(sql,user.getName());
    }

    public User findById(int id){
        String sql = "SELECT * FROM tb_user WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<User>(User.class),id);
        }catch (Exception e){
            throw new RuntimeException("未查询到此用户");
        }
    }

    public void deleteById(int id){
        jdbcTemplate.update("delete from tb_user where id=?",id);
    }

    public void updateById(int id,String newName){
        jdbcTemplate.update("update tb_user set money=? where id=?",newName,id);
    }
}
