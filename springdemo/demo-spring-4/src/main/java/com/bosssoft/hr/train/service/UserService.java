package com.bosssoft.hr.train.service;

import com.bosssoft.hr.train.bean.User;
import com.bosssoft.hr.train.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

public class UserService {

    @Autowired
    private UserDao userDao;

    public void addUser(User user){
        userDao.addUser(user);
    }
    public User findById(int id){
        return userDao.findById(id);
    }
    public void deleteById(int id){
        userDao.deleteById(id);
    }
    public void updateById(int id,String newName){
        userDao.updateById(id,newName);
    }
}
