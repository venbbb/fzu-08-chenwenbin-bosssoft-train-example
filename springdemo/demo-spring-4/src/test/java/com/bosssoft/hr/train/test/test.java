package com.bosssoft.hr.train.test;

import com.bosssoft.hr.train.bean.User;
import com.bosssoft.hr.train.service.UserService;
import org.junit.Test;

public class test {

    @Test
    public void addUser(){
        UserService userService = new UserService();
        User user = new User(20,"venb");
        userService.addUser(user);
    }

    @Test
    public User findById(int id){
        UserService userService = new UserService();
        return userService.findById(1);
    }

    @Test
    public void deleteById(int id){
        UserService userService = new UserService();
        userService.deleteById(1);
    }

    @Test
    private void updateById(){
        UserService userService = new UserService();
        userService.updateById(2,"iris");
    }
}
