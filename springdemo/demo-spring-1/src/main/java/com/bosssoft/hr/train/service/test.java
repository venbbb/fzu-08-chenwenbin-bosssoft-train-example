package com.bosssoft.hr.train.service;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


public class test {
    public static void main(String[] args) {
//        Resource resource = new ClassPathResource("application.xml");
//        BeanFactory bc = new XmlBeanFactory(resource);
//        System.out.println("创建容器完成");
//        UserService userService = bc.getBean("UserService",UserService.class);

        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("application.xml");
        ac.close();
    }
}
