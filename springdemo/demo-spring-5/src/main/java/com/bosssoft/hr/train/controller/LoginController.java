package com.bosssoft.hr.train.controller;

import com.bosssoft.hr.train.MyAnno;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @RequestMapping("login")
    @MyAnno(action = "用户登录",desc = "日志测试")
    public String login(){
        return "success";
    }
}
