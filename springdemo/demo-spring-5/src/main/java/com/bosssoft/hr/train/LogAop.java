package com.bosssoft.hr.train;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class LogAop {

    @Autowired
    private HttpServletRequest request;

    @Pointcut("execution(* com.bosssoft.hr.train.controller.*.*(..))")
    private void pt1(){};

    @Around("pt1()")
    public Object log(ProceedingJoinPoint pjp) throws NoSuchMethodException {


        //1. 获取访问者的ip
        String ip = request.getRemoteAddr();
        //2.获取访问时间
        String visitTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS").format(new Date());
        //3.参数内容
        /**
         * executionClass切入点类别  executionMethod切入点方法
         */
        Class executionClass = pjp.getTarget().getClass();
        Method executionMethod =null;
        String methodName = pjp.getSignature().getName();
        Object[] args = pjp.getArgs();
        if (args == null || args.length == 0) {
            executionMethod = executionClass.getMethod(methodName);
        } else {
            Class[] classArgs = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                classArgs[i] = args[i].getClass();
            }
            executionMethod = executionClass.getMethod(methodName, classArgs);
        }
        MyAnno anno = executionMethod.getAnnotation(MyAnno.class);
        Object returnValue = null;
        Long executionTime = null;
        try {
            //4.获取执行时长与应答内容
            Long startTime = System.currentTimeMillis();
            returnValue = pjp.proceed(args);
            executionTime = System.currentTimeMillis()-startTime;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        Map logMap = new HashMap();
        logMap.put("访问者ip",ip);
        logMap.put("访问时间",visitTime);
        logMap.put("访问的方法",anno.action());
        logMap.put("请求耗时",executionTime);
        logMap.put("应答内容",returnValue);
        System.out.println(logMap);
        return returnValue;
    }


}
