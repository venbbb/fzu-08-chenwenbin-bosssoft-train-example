package com.bosssoft.hr.train;

import com.bosssoft.hr.train.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserService userService = context.getBean(UserService.class);

        System.out.println("第一次查询");
        System.out.println(userService.getUser());
        System.out.println("第二次查询");
        System.out.println(userService.getUser());
    }
}
