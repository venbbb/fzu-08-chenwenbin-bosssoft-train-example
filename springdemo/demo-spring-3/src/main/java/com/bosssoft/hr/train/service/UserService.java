package com.bosssoft.hr.train.service;

import com.bosssoft.hr.train.bean.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

public class UserService {
    @Cacheable(value = "users", key = "#save")
    public void saveUser(User user){
        System.out.println("saveUser: "+user.getName());
    }

    @Cacheable(value = "users", key = "#update")
    public void updateUser(User user){
        System.out.println("updateUser: "+user.getName());
    }

    @CachePut("users")
    public User getUser(){
        return new User("testUser");
    }

    @CacheEvict(value="users", beforeInvocation=true, allEntries = true)
    public void deleteUsers(Integer id) {
        System.out.println("delete user by id: " + id);
    }
}
