package com.bosssoft.hr.train.bean;

import org.springframework.cache.annotation.Cacheable;

@Cacheable("cache")
public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
