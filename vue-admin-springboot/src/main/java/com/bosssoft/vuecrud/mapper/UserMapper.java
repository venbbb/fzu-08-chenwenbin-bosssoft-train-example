package com.bosssoft.vuecrud.mapper;

import com.bosssoft.vuecrud.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user")
    List<User> findAll();

    @Select("select * from user where id=#{id}")
    User findById(int id);

    @Delete("delete from user where id=#{id}")
    int deleteById(int id);

    @Insert("insert into user (id,name,remark) values (#{id},#{name},#{remark})")
    int insertUser(User user);

    @Update("update user set name=#{name}, remark=#{remark} where id=#{id}")
    int updateUser(Integer id,String name,Integer remark);

}
