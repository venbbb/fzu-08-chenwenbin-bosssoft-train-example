package com.bosssoft.vuecrud.controller;

import com.bosssoft.vuecrud.mapper.UserMapper;
import com.bosssoft.vuecrud.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserMapper userMapper;

    @GetMapping("/api/userlist")
    public List<User> findAll(){
        List<User> users = userMapper.findAll();
        return users;
    }

    @PostMapping("/api/add")
    public int addUser(@RequestParam(name = "id")Integer id,
                       @RequestParam(name = "name")String name,
                       @RequestParam(name = "remark")Integer remark){
        User user = new User(id,name,remark);
        return userMapper.insertUser(user);
    }

    @PostMapping("/api/del")
    public int delUser(@RequestParam(name = "id")Integer id){
        return userMapper.deleteById(id);
    }

    @PostMapping("/api/update")
    public int updateUser(@RequestParam(name = "name")String name,
                          @RequestParam(name = "id")Integer id,
                          @RequestParam(name = "remark")Integer remark){
        return userMapper.updateUser(id,name,remark);
    }

    @PostMapping("/api/find")
    public User findUser(@RequestParam(name = "id")Integer id){
        return userMapper.findById(id);
    }
}
