package com.bosssoft.vuecrud.pojo;

public class User {
    private Integer id;
    private String name;
    private Integer remark;

    public User(Integer id, String name, Integer remark) {
        this.id = id;
        this.name = name;
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", remark=" + remark +
                '}';
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRemark(Integer remark) {
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getRemark() {
        return remark;
    }
}
