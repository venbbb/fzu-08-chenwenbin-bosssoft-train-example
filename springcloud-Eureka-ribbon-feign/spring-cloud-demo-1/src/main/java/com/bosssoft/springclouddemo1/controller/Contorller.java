package com.bosssoft.springclouddemo1.controller;

import com.bosssoft.springclouddemo1.pojo.User;
import com.bosssoft.springclouddemo1.service.ServiceVue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Contorller {

    @Autowired
    ServiceVue serviceVue;

    @GetMapping("/api/userlist")
    public List<User> findAll(){
        return serviceVue.findAll();
    }
}
