package com.bosssoft.springclouddemo1.service;


import com.bosssoft.springclouddemo1.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "vue-crud",fallback = ServiceVueImpl.class)
public interface ServiceVue {

    @GetMapping("/api/userlist")
    List<User> findAll();

}
