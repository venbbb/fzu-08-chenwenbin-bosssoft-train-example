package com.bosssoft.dbms;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

public interface CallProcedure {

    @Select({  "call proc_init_sysdata(#{user_num,mode=IN,jdbcType=INTEGER},"
            + "#{role_num,mode=IN,jdbcType=INTEGER})"})
    @Options(statementType= StatementType.CALLABLE)
    Integer callProcedure(@Param("user_num") int userNum, @Param("role_num")int roleNum);
}
