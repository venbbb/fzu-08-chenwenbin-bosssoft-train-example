package com.bosssoft.vuecrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class VueCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueCrudApplication.class, args);
    }

}
